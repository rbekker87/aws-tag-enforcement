import sys
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText

def notification(from_address, to_address):
    print from_address
    print to_address

def mailer(from_address, to_address, subject, body):
    msg = MIMEMultipart()
    msg['From'] = from_address
    msg['To'] = to_address
    msg['Subject'] = "Notification: {}".format(subject)
    body_detail = "{}".format(body)
    msg.attach(MIMEText(body_detail, 'plain'))
    server = smtplib.SMTP('email-smtp.eu-west-1.amazonaws.com', 587)
    server.starttls()
    server.login("USER", "PASS")
    text = msg.as_string()
    server.sendmail(from_address, to_address, text)
    server.quit()

if len(sys.argv) == 5:

    from_address = sys.argv[1]
    to_address = sys.argv[2]
    subject = sys.argv[3]
    body = sys.argv[4]

else:
    print("ArgumentException: %s from_address to_address subject body " % (sys.argv[0]))
    exit(1)

try:
    mailer(from_address, to_address, subject, body)
    print("Mail Sent")

except Exception as e:
    print(e)