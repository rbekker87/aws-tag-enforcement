from pymongo import MongoClient
import botocore.session
import boto3

# mongodb
uri = 'mongodb://127.0.0.1:27017/tag_enforcement'
client = MongoClient(uri)
mongo_col = client['tag_enforcement']['metadata']

# setting region, profile from credential provider
session = boto3.Session(
        region_name='eu-west-1',
        profile_name='myprofile'
        )

# declare ec2
ec2 = session.client('ec2')

# get a dictionary of all the instances
response = ec2.describe_instances()

# get the number of objects
obj_number = len(response['Reservations'])

# print the instance-id's with no tags
for objects in xrange(obj_number):
    try:
        z = response['Reservations'][objects]['Instances'][0]['Tags'][0]['Key']
    except KeyError as e:
        untagged_instanceid = response['Reservations'][objects]['Instances'][0]['InstanceId']
        untagged_state = response['Reservations'][objects]['Instances'][0]['State']['Name']
        #mongo_col.insert({"InstanceId": untagged_instance, "RunningState": untagged_state})
        print("InstanceID: {}, RunningState: {}".format(untagged_instanceid, untagged_state))


# $ python runner.py
# InstanceID: i-03b7608f2a9784cef, RunningState: running
# InstanceID: i-01383968e138c20a4, RunningState: running
# InstanceID: i-07ad00e454439f0c0, RunningState: running